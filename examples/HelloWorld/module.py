from pypsm import PSModule, PSFunction, PSArgument, pstypes

mod = PSModule(__file__)

def send_greetings(place):
    print('Hello %s' % place)


mod.addfunc(PSFunction('Send-Greetings', send_greetings,
                       PSArgument('place', pstypes.psstring)))


mod()
