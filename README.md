# pypsm
Write powershell modules with a real scripting language, Python

### Example module

We want to create a module named HelloWorld, that contains 1 Function, Send-Greeting with
A required argument -place and an optional argument -greeting with a default value of __Hello__
The function schould print _greeting_ _place_. In PowerShell Send-Greeting -place World schould print Hello World

To write a function like that, first you create a directory where your PowerShell module lives, for example
\Code\HelloWorld, in this directory, you create a subdirectory, where the python modules are stored, this schould be
called pymodules

    mkdir \HelloWorld
    mkdir \HelloWorld\pymodules
    cd \HelloWorld

To install the pypsm python package, you can use pip, to store pypsm in pymodules use the following command

    pip install pypsm -t pymodules

Other modules you can install in the same way

To define create a file named module.py (or how ever you want) with the following content

	from pypsm import PSModule
	from pypsm.pstypes import psstring
	
	mod = PSModule(__file__)
	
	# Notice that function names are converted to powershell style (Send-Greeting)

	@mod.function 
	def send_greeting(place: psstring, greeting: psstring='Hello'):
        """Documentation goes here"""
	    print('{} {}'.format(greeting, place))

	mod()

	
If you prefer a more verbose way, you can create the objects by yourself

    from pypsm import PSModule, PSFunction, PSArgument, pstypes


    def send_greeting(greeting, place):
        print('{} {}'.format(greeting, place))

    mod = PSModule(__file__)
    
    mod.addfunc(PSFunction('Send-Greeting', send_greeting, 
                            PSArgument('place', pstypes.psstring),
                            PSArgument('greeting', pstypes.psstring, 'Hello'))) # Specify a default value
    
    mod()

Finally, you can create a .psm1 file, that is valid PowerShell. __In your module manifest, you
must specify the module Python3 as a dependency__. 

To create the psm1 file, use the following command:

    set PYMODULES=%CD%\pymodules # Windows
    export PYMODULES=`pwd`/pymodules # Linux

    modules.py --psm HelloWorld.psm1 # Windows
    ./modules.py --psm HelloWorld.psm1 # Linux

All you have to do now is to create a module manifest and publish your module to the PSGallery or to your own repository. 